#!/usr/bin/env bash

echo " --> Running Emerge Web Synchronization"
emerge-webrsync
echo " --> Installing MAVProxy Dependancies"
emerge --autounmask-write -j6 -v opencv wxGTK matplotlib pygame lxml pyyaml dev-python/pip
echo " --> Installing MAVProxy"
python2 -m pip install --user MAVProxy
echo "export PATH=$PATH:$HOME/.local/bin" >> ~/.bashrc
rc-update add local default
echo " --> Cleaning up"
emerge --depclean --with-bdeps=n
rm -fr /usr/portage/*
rm -fr /tmp/*
rm -fr /var/tmp/*
